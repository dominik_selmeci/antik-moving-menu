/*
 * Author: Bc. Dominik Šelmeci
 * Last update: 2015-07-19
 * Version: 1.0.0
 * 
 * SK: Ukážka jednoduchého prepínania menu boxov vľavo/vpravo
 * ak box dosiahne svojho konca posunie sa celý blok
 * v prípade, že v smere do ktorého sa má celý blok posunúť chýba box, tak sa doplní 
 * (točí sa to v kruhu) - prvý sa vloží nakoniec / posledný sa vloží na začiatok
 * 
 * Program bude korektne fubgovať len ak bude mať k dispozícií aspoň 7 tlačidiel
 * Ikony boli použité z open source knižnice FontAwesome
 */

/*
 * SK: Funkcia, ktorá korektne vypočíta bounding box ak je element transformovaný
 * limitácia: Nedokáže korektne spočítať ak je transformovaný element vložený do 
 * transformovaného parenta
 */
function transformedBoundingBox(el){
	var bb  = el.getBBox(),
		svg = el.ownerSVGElement,
		m   = el.getTransformToElement(el.parentNode);

	// Create an array of all four points for the original bounding box
	var pts = [
		svg.createSVGPoint(), svg.createSVGPoint(),
		svg.createSVGPoint(), svg.createSVGPoint()
	];
	pts[0].x=bb.x;          pts[0].y=bb.y;
	pts[1].x=bb.x+bb.width; pts[1].y=bb.y;
	pts[2].x=bb.x+bb.width; pts[2].y=bb.y+bb.height;
	pts[3].x=bb.x;          pts[3].y=bb.y+bb.height;

	// Transform each into the space of the parent,
	// and calculate the min/max points from that.    
	var xMin=Infinity,xMax=-Infinity,yMin=Infinity,yMax=-Infinity;
	pts.forEach(function(pt){
		pt = pt.matrixTransform(m);
		xMin = Math.min(xMin,pt.x);
		xMax = Math.max(xMax,pt.x);
		yMin = Math.min(yMin,pt.y);
		yMax = Math.max(yMax,pt.y);
	});

	// Update the bounding box with the new values
	bb.x = xMin; bb.width  = xMax-xMin;
	bb.y = yMin; bb.height = yMax-yMin;
	return bb;
}


/*
 * Objekt slúžiaci pre inicializáciu a posun medzi blokmi
 */
var Boxes = 
{
	boxes_move_x : 0,
	
	inicializeBoxesPosition: function()
	{
		var icons = document.querySelectorAll(".icon");
		
		Array.prototype.forEach.call(icons, function(el, i){
			el.setAttribute("transform", "translate(60, 530)");
		});
	},
	inicialize: function()
	{
		this.boxes = document.querySelectorAll("#boxes .box-group");
		this.first = this.boxes[0];
		this.last = this.boxes[this.boxes.length-1];
		this.boxes_anim = document.querySelectorAll("#boxes-area-anim")[0];
					
		this.el = document.querySelectorAll("#boxes .box-group.active")[0];
		this.el_next = this.el.nextElementSibling;
		this.el_before = this.el.previousElementSibling;
		this.bbox = transformedBoundingBox(this.el);
		this.boxes_area = document.querySelectorAll("#boxes")[0];
	},
	animateBoxesArea: function(offset)
	{
		this.boxes_anim.setAttribute("from", this.boxes_move_x);
		this.boxes_move_x += offset;
		this.boxes_anim.setAttribute("to", this.boxes_move_x);
		this.boxes_anim.beginElement();
		
		this.boxes_area.setAttribute("transform", "translate("+this.boxes_move_x+",0)");
	},
	animateBox: function(el_from, el_to)
	{
		var animOut = el_from.querySelectorAll(".fadeOut")[0];
		var animIn = el_to.querySelectorAll(".fadeIn")[0];
		animOut.beginElement();
		animIn.beginElement();
		
		el_from.setAttribute("fill", "#f1e6db");
		el_to.setAttribute("fill", "#ceaf78");
	},
	moveRight: function()
	{
		this.inicialize();
		
		if ((this.bbox.x+this.boxes_move_x) >= 1070) //ak aktívne ozn. objekt je na konci
		{
			if (!this.el_next || this.el_next == null)
			{					
				this.last_move = parseInt(this.last.getAttribute("transform").replace("translate", "").replace("(","").replace(")",""));

				this.boxes_area.appendChild(this.first);
				this.first.setAttribute("transform", "translate("+(this.last_move+212)+")");
				this.el_next = this.first;
			}

			this.animateBoxesArea(-212);
		} 

		if (this.el.classList) {
			this.el.classList.remove('active');
			this.el_next.classList.add('active');
		} else {
			this.el.className = "box-group";
			this.el_next.className = "box-group active";
		}

		this.animateBox(this.el, this.el_next);	
	},
	moveLeft: function()
	{
		this.inicialize();
		
		if ((this.bbox.x+this.boxes_move_x) <= 30) //ak aktívne ozn. objekt je na začiaku
		{
			if (!this.el_before || this.el_before === null)
			{
				this.first_move = parseInt(this.first.getAttribute("transform").replace("translate", "").replace("(","").replace(")",""));

				this.boxes_area.insertBefore(this.last, this.boxes_area.firstChild);

				this.last.setAttribute("transform", "translate("+(this.first_move-212)+")");
				this.el_before = this.last;
			}

			this.animateBoxesArea(212);
		}
			
		if (this.el.classList) {
			this.el.classList.remove('active');
			this.el_before.classList.add('active');
		} else {
			this.el.className = "box-group";
			this.el_before.className = "box-group active";
		}

		this.animateBox(this.el, this.el_before);
	}
};


// keydown event listener
document.onkeydown = function keydown(e)
{
	var keypressed = e.keyCode == null ? e.charCode : e.keyCode;

	switch(keypressed) 
	{
		case 37: //left
			Boxes.moveLeft();
			break;
			
		case 39: //right
			Boxes.moveRight();
				
			break;
		case 917538:
			window.location.reload();
			break;
	}
};

// inicialize
Boxes.inicializeBoxesPosition();